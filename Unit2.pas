unit Unit2;

interface

uses  Controls, Windows, Graphics, ExtCtrls, Types, Classes, Forms, {-}QDialogs, sysutils;

const
  CellWidth = 20;

type
  TWhoMoving = (wmNone, wmHuman, wmComputer, wmGamer1, wmGamer2); //��� ���

  TOrientation = (osHorizontal, osVertical);            // ��� - ���������� �������: ������������� ��� �����������

  TDeck = class(TObject)                                //����� - ������
    ALive: Boolean;                                     //������� �����
    constructor Create;                                 //�����������
    procedure Show(Canvas: TCanvas; x, y, dx: Integer); //����������� ������ �� ������� � �������� �����������
  end;

  TShip = class(TObject)                                //����� - �������
    Decks: array [1..4] of TDeck;                       //������
    CounDecks: Byte;                                    //���������� �����
    Orientation: TOrientation;                          //���������� �������
    Placement: TPoint;                                  //��������������� ������ ������ ������� � ������������� �����������
    constructor Create(cd: Byte; orient: TOrientation; pl: TPoint); //�����������
    destructor Destroy; override;                       //����������
    procedure Show(Canvas: TCanvas; x, y, dx: Integer); //����������� ������� �� ������� � �������� �����������
    function ALive: Boolean;                            //������� ����� �������
    procedure Crash(NomDeck: Byte);                     //��������� � ������
  end;

  TSea = class(TObject)                                 //����� - ����
    Ships: array [1..10] of TShip;                      //�������
    Placement: TPoint;                                  //�������������� ����
    WidthCell: Integer;                                 //������ ������
    Canvas: TCanvas;                                    //������, �� ������� ����� ������������ �����������
    FMap: array [1..10, 1..10] of Integer;              //����� ����: 0 - ��������� ������;
                                                        //            -1 - ������;
                                                        //            11-14 - ������, ���������� 1 ��������;
                                                        //            21-24 - ������, ���������� 2 ��������;
                                                        //                    � �.�.
                                                        //            101-104 - ������, ���������� 10 �������;
                                                        //            1100-1300 - ������, ���������� ��������� �������� 1 �������;
                                                        //            2100-2300 - ������, ���������� ��������� �������� 2 �������;
                                                        //                    � �.�.
                                                        //            10100-10300 - ������, ���������� ��������� �������� 10 �������;

    constructor Create(c: TCanvas; x, y, dx: Integer);  //�����������
    destructor Destroy; override;                       //����������
    procedure Show(NoShip: Boolean); overload;          //��������� ����
    procedure Show(x, y: Integer); overload;            //��������� ������, � ������� ��� �������
    procedure AutomaticShipCreate;                      //�������������� ����������� ��������
    procedure ManualShipCreate;                         //������ ����������� ��������
    function Bang(x, y: Integer): Integer;              //������� (������������ ��������� ��������)
    function GetMap(IndexI, IndexY: Integer): Integer;  //����� ��� ������ �� ��-�� Map
    procedure SetMap(IndexI, IndexY: Integer; AValue: Integer); //����� ��� ������ � ��-�� Map
    property Map[IndexI, IndexY: Integer]: Integer read GetMap write SetMap;  //�������� - �����
  end;

  TGamer = class(TObject)                               //����� ������������ ������
    Name: String;                                       //��� ������
    Points: Integer;                                    //����
    Turned: Boolean;                                    //����� ������
    Shoot: TPoint;                                      //������� ������
    constructor Create(nam: String);                    //�����������
    function Shooting(x, y: Integer): TPoint; virtual; abstract; //��� ������. ����� ������ ����������� �����,
                                                                  //������� ����� ��������� ���������� - ��������� ������� ���� (��� ��������),
                                                                  //� ���������� ���������� ����� ����������. ���� ���������� ����� ������ �����
                                                                  //���������� � ����������� �� ������ � ������������ � �������-��������
  end;

  THumanGamer = class(TGamer)                           //����� ������������� ������, ������� �� ������ ������������ ������
    constructor Create(nam: String);                    //�����������
    function Shooting(x, y: Integer): TPoint; override; //��� ������
  end;

  TComputerGamer = class(TGamer)                        //����� ������������� ������, ������� �� ������ ������������ ������
    constructor Create(nam: String);                    //�����������
    function Shooting(x, y: Integer): TPoint; override; //��� ������
  end;

  TGameProcess = class(TObject)                         //����� �������� ��������
    GameNow: Boolean;                                   //������� ����
    OurSea,                                             //���� ������� ����
    EnemySea: TSea;                                     //����� ������� ����
    Placement: TPoint;                                  //��������������� �������� ����
    Image: TImage;                                      //����� ��� ��������� ����� � ��������
    WhoMoving: TWhoMoving;                              //��� ���
    Gamers: array [1..2] of TGamer;                     //������. ������ ����������� �����-������, � ����� ����������� ����� � ��������� �����-�������
    constructor Create(Owner: TWinControl; pl: TPoint); //�����������
    destructor Destroy; override;                       //����������
    procedure GameStart;                                //������ ����
    function GameEnd: Byte;                             //��������� ����: 0 - �� ����������� ����;
                                                        //                1 - ������� �������;
                                                        //                2 - ������� ���������
    procedure TurnHumanGamer(Sender: TObject; Button: TMouseButton;
                             Shift: TShiftState; X, Y: Integer);           //��� ������-��������
    function Game: Byte;                                //���� 1 - ������� �������;
                                                        //     2 - ������� ���������
  end;

implementation

{ TDeck }

constructor TDeck.Create;
begin
  //�������� �������
  inherited Create;
  //��������� ��������� �������
  ALive := True;
end;

procedure TDeck.Show(Canvas: TCanvas; x, y, dx: Integer);
begin
  //���� ������ �������, �� ���� ������� - �������, ����� - ������
  if Alive then begin
    Canvas.Brush.Color := clGreen;
  end
  else begin
    Canvas.Brush.Color := clRed;
  end;
  //������ ������� � �������� �����������
  Canvas.Rectangle(x, y, x + dx, y + dx);
end;

{ TShip }

function TShip.ALive: Boolean;
var
  i: Byte;
begin
  //��������� ��� ������ �������, � ���� ���� �� ���� �� �� �������, �� ��������� - True
  Result := False;
  for i := 1 to CounDecks do begin
    Result := Result or Decks[i].ALive;
  end;
end;

procedure TShip.Crash(NomDeck: Byte);
begin
  //�������� �������� ������
  Decks[NomDeck].ALive := False;
end;

constructor TShip.Create(cd: Byte; orient: TOrientation; pl: TPoint);
var
  i: Byte;
begin
  //�������� �������
  inherited Create;
  //��������� ��������� �������
  CounDecks := cd;
  Orientation := orient;
  Placement := pl;
  //�������� ��������� ��������
  for i := 1 to CounDecks do begin
    Decks[i] := TDeck.Create;
  end;
end;

destructor TShip.Destroy;
var
  i: Byte;
begin
  //����������� ��������� ��������
  for i := 1 to CounDecks do begin
    Decks[i].Free;
  end;
  inherited;
end;

procedure TShip.Show(Canvas: TCanvas; x, y, dx: Integer);
var
  i: Byte;
begin
  //� ����� �������������� ��� ������
  for i := 1 to CounDecks do begin
    case Orientation of
    osHorizontal: begin
//                    Decks[i].Show(Canvas, x + dx * (i - 1), y, dx);
                    Decks[i].Show(Canvas, x + (Placement.X - 1) * dx + dx * (i - 1), y + (Placement.Y - 1) * dx, dx);
                  end;
    osVertical:   begin
                    Decks[i].Show(Canvas, x + (Placement.X - 1) * dx , y + (Placement.Y - 1) * dx + dx * (i - 1), dx);
                  end;
    end;
  end;
end;

{ TSea }

procedure TSea.AutomaticShipCreate;
var
  k, num, x, y: Byte;
  i: Integer;
  ori, NoCross: Boolean;
  orient: TOrientation;
begin
  //��������� ��������
  k := 1;
  repeat
    //����������� ���������� ����� � �������
    case k of
    1    : num := 4;
    2, 3 : num := 3;
    4..6 : num := 2;
    7..10: num := 1;
    end;
    //����������� ���������� �������
    ori := Random > 0.5;
    if ori then begin
      orient := osHorizontal;
    end
    else begin
      orient := osVertical;
    end;
    //����������� ��������� �������
    if ori then begin
       x := Random(10 - num + 1) + 1;
       y := Random(10) + 1;
     end
     else begin
       x := Random(10) + 1;
       y := Random(10 - num + 1) + 1;
     end;
     //�������� �� ����������� � ������� ���������, � ����� �� ��������� �� ����� ������ �������
     NoCross := True;
     if ori then begin
       for i := -1 to num do begin
         if (Map[x+i, y-1] <> 0) or (Map[x+i, y] <> 0) or (Map[x+i, y+1] <> 0) then begin
           NoCross := False;
         end;
       end;
     end
     else begin
       for i := -1 to num do begin
         if (Map[x-1, y+i] <> 0) or (Map[x, y+i] <> 0) or (Map[x+1, y+i] <> 0) then begin
           NoCross := False;
         end;
       end;
     end;
     //���� ��� ����������� � ������� ���������
     if NoCross then begin
       //��������� ����� ���������� ������� ������� � ������
       if ori then begin
         for i := 0 to num - 1 do begin
           Map[x+i, y] := k * 10 + i + 1;
         end;
       end
       else begin
         for i := 0 to num - 1 do begin
           Map[x, y+i] := k * 10 + i + 1;
         end;
       end;
       //�������� �������
       Ships[k] := TShip.Create(num, orient, Point(x, y));
       k := k + 1;
     end;
   until k > 10;
end;

function TSea.Bang(x, y: Integer): Integer;
var
  NomShip: Byte;   //����� �������, � ������� ������
  NomDeck: Byte;   //����� ������ �������, � ������� ������
  ii, jj: Byte;
begin
  //���������� ��, ��� ��������� � ���� ������
  Result := Map[x, y];
  //������������ ������
  if Map[x, y] < 0 then begin    //���� � ������ ��� ��� �������
    //�� ������ �� ������
    exit;
  end;
  if Map[x, y] = 0 then begin    //���� ������ ���� ��������
    //������������� ������� �������
    Map[x, y] := -1;
    //������ ����� � ������
    Show(x, y);
    //������� �� ���������
    exit;
  end;
  if Map[x, y] > 0 then begin    //���� � ������ �����-�� �������
    if Map[x, y] < 1000 then begin     //���� � ������ �����-�� ������� � ���������� �������
      //�� ��������� ������ ������ �������
      //���������� ����� ������� � ����� ������, � ������� ������
      NomShip := Map[x, y] div 10;
      NomDeck := Map[x, y] mod 10;
      //���������, ��� ��� ������ �������
      //������� � ����� ����
      Map[x, y] := Map[x, y] * 100;
      //����� � ��� �������
      Ships[NomShip].Crash(NomDeck);
      //�������������� ������
      Ships[NomShip].Decks[NomDeck].Show(Canvas, Placement.X, Placement.Y, WidthCell);   //������ ������, ��� �� ������ ���������:
                                                                     //�� �� ������ �������� �������� ����� ������, ����� �������
      //��������� ������ �������
      if not Ships[NomShip].ALive then begin  //���� ������� ������
        //�� ������ ����� ������ ��� �����
        case Ships[NomShip].Orientation of
        osHorizontal: begin
                        Map[Ships[NomShip].Placement.X, Ships[NomShip].Placement.Y-1] := -1;
                        Show(Ships[NomShip].Placement.X, Ships[NomShip].Placement.Y - 1);
                        for jj := Ships[NomShip].Placement.Y - 1 to Ships[NomShip].Placement.Y + Ships[NomShip].CounDecks do begin
                          Map[Ships[NomShip].Placement.X-1, jj] := -1;
                          Show(Ships[NomShip].Placement.X - 1, jj);
                          Map[Ships[NomShip].Placement.X+1, jj] := -1;
                          Show(Ships[NomShip].Placement.X + 1, jj);
                        end;
                        Map[Ships[NomShip].Placement.X, Ships[NomShip].Placement.Y+Ships[NomShip].CounDecks] := -1;
                        Show(Ships[NomShip].Placement.X, Ships[NomShip].Placement.Y + Ships[NomShip].CounDecks);
                      end;
        osVertical  : begin
                        Map[Ships[NomShip].Placement.X-1, Ships[NomShip].Placement.Y] := -1;
                        Show(Ships[NomShip].Placement.X - 1, Ships[NomShip].Placement.Y);
                        for ii := Ships[NomShip].Placement.X - 1 to Ships[NomShip].Placement.X + Ships[NomShip].CounDecks do begin
                          Map[ii, Ships[NomShip].Placement.Y-1] := -1;
                          Show(ii, Ships[NomShip].Placement.Y - 1);
                          Map[ii, Ships[NomShip].Placement.Y+1] := -1;
                          Show(ii, Ships[NomShip].Placement.Y + 1);
                        end;
                        Map[Ships[NomShip].Placement.X+Ships[NomShip].CounDecks, Ships[NomShip].Placement.Y] := -1;
                        Show(Ships[NomShip].Placement.X + Ships[NomShip].CounDecks, Ships[NomShip].Placement.Y);
                      end;
        end;
        //������� �� ���������
        exit;
      end;
      //������� �� ���������
      exit;
    end
    else begin                         //���� � ������ �����-�� ������� � �������� �������
      //�� ������ �� ������
      exit;
    end;
  end;
end;

constructor TSea.Create(c: TCanvas; x, y, dx: Integer);
begin
  //�������� �������
  inherited Create;
  //������� ��������� ������
  Canvas := c;
  Placement := Point(x, y);
  WidthCell := dx;
  AutomaticShipCreate;
end;

destructor TSea.Destroy;
var
  i: Byte;
begin
  //����������� ��������
  for i := 1 to 10 do begin
    Ships[i].Free;
  end;
  inherited;
end;

function TSea.GetMap(IndexI, IndexY: Integer): Integer;
begin
  //��������� �������
  if ((IndexI > 0) and (IndexI < 11) or (IndexY > 0) and (IndexY < 11)) then begin  //���� ������ � ��������
    //�� ������ �������� �� �������
    Result := FMap[IndexI, IndexY];
  end
  else begin
    Result := 0;
  end;
end;

procedure TSea.ManualShipCreate;
begin
  //���� �� �����������
end;

procedure TSea.Show(NoShip: Boolean);
var
  i, j: Byte;
begin
  //������ ����
  //������ �������
  Canvas.Brush.Color := clBlue;
  Canvas.Rectangle(Placement.X, Placement.Y, Placement.X + 10 * WidthCell, Placement.Y + 10 * WidthCell);
  //������ ������
  for i := 0 to 10 do begin
    //������ �������������� �����
    Canvas.MoveTo(Placement.X, Placement.Y + i * WidthCell);
    Canvas.LineTo(Placement.X + WidthCell * 10, Placement.Y + i * WidthCell);
    //������ ������������ �����
    Canvas.MoveTo(Placement.X + i * WidthCell, Placement.Y);
    Canvas.LineTo(Placement.X + WidthCell * i, Placement.Y + 10 * WidthCell);
  end;
  //������ �������
  Canvas.Brush.Color := clBlack;
  for i := 1 to 10 do begin
    for j := 1 to 10 do begin
      if Map[i,j] = -1 then begin
        Show(i, j);
      end;
    end;
  end;
  //������ �������
  if not NoShip then begin    //���� ���� ���������� �������
    //�� ������������ �������
    for i := 1 to 10 do  begin
      Ships[i].Show(Canvas, Placement.X, Placement.Y, WidthCell);
    end;
  end
  else begin
    //������ ������ �������� ������� ��� ������
    for i := 1 to 10 do begin
      for j := 1 to Ships[i].CounDecks do begin
        if not Ships[i].Decks[j].ALive then begin
          case Ships[i].Orientation of
          osHorizontal: begin
                          Ships[i].Decks[j].Show(Canvas, Placement.X + (Ships[i].Placement.X - 1 + j) * WidthCell, Placement.Y + (Ships[i].Placement.Y - 1) * WidthCell, WidthCell);
                        end;
          osVertical:   begin
                          Ships[i].Decks[j].Show(Canvas, Placement.X + (Ships[i].Placement.X - 1) * WidthCell, Placement.Y + (Ships[i].Placement.Y - 1 + j) * WidthCell, WidthCell);
                        end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TSea.SetMap(IndexI, IndexY, AValue: Integer);
begin
  //��������� �������
  if ((IndexI > 0) and (IndexI < 11) or (IndexY > 0) and (IndexY < 11)) then begin  //���� ������ � ��������
    //�� ���������� ����� �������� � ������
    FMap[IndexI, IndexY] := AValue;
  end;
end;

procedure TSea.Show(x, y: Integer);
begin
  if ((x > 0) and (x < 11) or (y > 0) and (y < 11)) then begin  //���� ������ � ��������
    //������ ������ ������, �������� �������������� ������������
    Canvas.Brush.Color := clBlack;
    Canvas.Ellipse(Placement.X + (x - 1) * WidthCell + Trunc(WidthCell / 4), Placement.Y + (y - 1) * WidthCell + Trunc(WidthCell / 4),
                   Placement.X + x * WidthCell - Trunc(WidthCell / 4), Placement.Y + y * WidthCell - Trunc(WidthCell / 4));
  end;
end;

{ TGameProcess }

constructor TGameProcess.Create(Owner: TWinControl; pl: TPoint);
begin
  Placement := pl;
  Image := TImage.Create(nil);
  Image.Parent := Owner;
  Image.Left := Placement.X;
  Image.Top := Placement.Y;
  Image.Width := 2 * 10 * CellWidth + 100;
  Image.Height := 10 * CellWidth + 50;
  Image.OnMouseUp := TurnHumanGamer;
  GameNow := False;
  WhoMoving := wmNone;
end;

destructor TGameProcess.Destroy;
begin
  OurSea.Free;
  EnemySea.Free;
  Image.Free;
  Gamers[1].Free;
  Gamers[2].Free;
  inherited;
end;

function TGameProcess.Game: Byte;
var
  temp: Byte;
begin
  //���� ���� ��� ��������
  if GameNow then begin
    case WhoMoving of
    wmHuman:    begin
                  Gamers[1].Turned := False;
                  //���� �������� ��������
                  while not Gamers[1].Turned do begin
                    Application.ProcessMessages;
                  end;
                  //��������� ���� ����� �������
                  if EnemySea.Map[Gamers[1].Shoot.X,Gamers[1].Shoot.Y] <> 0 then begin
                    //���-�� ����
                    ShowMessage('�����');
                  end
                  else begin
                    //������ ��� � ���� ������
                    //������ �����
                    EnemySea.Map[Gamers[1].Shoot.X,Gamers[1].Shoot.Y] := -1;
                    EnemySea.Show(Gamers[1].Shoot.X,Gamers[1].Shoot.Y);
                  end;
                end;
    wmComputer: begin
                //  Shoot := Gamers[2].Shooting(0, 0);
                end;
    end;





    //����������� ��������� ����
    temp := GameEnd;
    if temp > 0 then begin
      Result := temp;
      GameNow := False;
    end
    else begin
      Game;
    end;
  end;
end;

function TGameProcess.GameEnd: Byte;
begin
  //���� ���� �� �������������
  Result := 0;
end;

procedure TGameProcess.GameStart;
begin
  //������ ����
  GameNow := True;
  //�������� �����
  OurSea := TSea.Create(Image.Canvas, 10, 10, CellWidth);
  OurSea.Show(False);
  EnemySea := TSea.Create(Image.Canvas, Trunc(Image.Width / 2), 10, CellWidth);
  EnemySea.Show(True);
  //�������� �������
  Gamers[1] := THumanGamer.Create('�������');
  Gamers[2] := TComputerGamer.Create('���������');
  //����������, ��� ������ �����
  if Random > 0.5 then begin
    WhoMoving := wmComputer;
  end
  else begin
    WhoMoving := wmHuman;
  end;
    WhoMoving := wmHuman;     ///////////////////////////////////////////////////
  //������
  Game;
end;

procedure TGameProcess.TurnHumanGamer(Sender: TObject; Button: TMouseButton;
                                      Shift: TShiftState; X, Y: Integer);
var
  xx, yy: Integer;
begin
  //���� ���� ����
  if GameNow then begin
    //�� ��� �� �������� ���
    //��������� ��������� � ����� ����
    if ((X >= EnemySea.Placement.X) and (X <= EnemySea.Placement.X + 10 * CellWidth) and
        (Y >= EnemySea.Placement.Y) and (Y <= EnemySea.Placement.Y + 10 * CellWidth)) then begin
      //����� � ����� ������ ������
      xx := ((X - EnemySea.Placement.X) div CellWidth) + 1;
      yy := ((Y - EnemySea.Placement.Y) div CellWidth) + 1;
      Gamers[1].Shoot := Gamers[1].Shooting(xx, yy);
    end;
  end;
end;

{ TGamer }

constructor TGamer.Create(nam: String);
begin
  inherited Create;
  Name := nam;
  Points := 0;
end;

{ THumanGamer }

constructor THumanGamer.Create(nam: String);
begin
  inherited Create(nam);
end;

function THumanGamer.Shooting(x, y: Integer): TPoint;
begin
  Result := Point(x, y);
  Turned := True;
end;

{ TComputerGamer }

constructor TComputerGamer.Create(nam: String);
begin
  inherited Create(nam);
end;

function TComputerGamer.Shooting(x, y: Integer): TPoint;
begin

  Result := Point(x, y);

end;

end.
